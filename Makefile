# Copyright (C) 2023 Haik Khachatrian <haiksemail@gmail.com>
#
# This file is part of tpm-utils.
#
# tpm-utils is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# tpm-utils is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# tpm-utils. If not, see <https://www.gnu.org/licenses/>.

include config.mk

install:
	install -Dm 755 tpm-copy $(DESTDIR)$(PREFIX)/bin/tpm-copy
	install -Dm 755 tpm-generate $(DESTDIR)$(PREFIX)/bin/tpm-generate
	install -Dm 755 tpm-otp $(DESTDIR)$(PREFIX)/bin/tpm-otp
	install -Dm 755 tpm-ls $(DESTDIR)$(PREFIX)/bin/tpm-ls

uninstall:
	rm $(DESTDIR)$(PREFIX)/bin/tpm-copy
	rm $(DESTDIR)$(PREFIX)/bin/tpm-generate
	rm $(DESTDIR)$(PREFIX)/bin/tpm-otp
	rm $(DESTDIR)$(PREFIX)/bin/tpm-ls

.PHONY: install uninstall
