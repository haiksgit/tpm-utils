# Copyright (C) 2023 Haik Khachatrian <haiksemail@gmail.com>
#
# This file is part of tpm-utils.
#
# tpm-utils is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# tpm-utils is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# tpm-utils. If not, see <https://www.gnu.org/licenses/>.

PREFIX = /usr/local
